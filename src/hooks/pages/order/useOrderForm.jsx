import useModal from "@/hooks/commons/useModal";
import { useAddOrderService } from "@/services/orders/orderService";
import { useForm } from "react-hook-form";

const useOrderForm = ({ order, onEnd }) => {
  const {
    reset,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();

  const addOrderMutation = useAddOrderService();

  const onSubmit = async (data) => {
    delete order.id;
    delete order.number;
    const payload = {
      order: {
        shippingMethod: "Express",
        email: "support@mail.com",
        totals: {},
        status: {
          id: "1",
        },
        locationId: "1",
        reference: "active",
        payment: {
          method: "credit card",
        },
        billingAddress: {
          id: "1",
        },
        shippingAddress: {
          id: "1",
        },
        items: [
          // ...order.items,
          {
            name: data.name,
            quantity: data.quantity,
            price: data.price,
            fulfillment: {
              status: "active",
            },
          },
        ],
        note: "Note",
        dates: {},
      },
    };

    await addOrderMutation.mutateAsync(payload);
    reset();
    onEnd()
  };

  return {
    handleSubmit,
    control,
    errors,
    onSubmit,
  };
};

export default useOrderForm;
