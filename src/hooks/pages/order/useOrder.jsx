import {
  useGetOrderByIdService,
} from "@/services/orders/orderService";

const useOrder = ({ row, onEnd, isUpdate = false }) => {
  const { data: order, isLoading: orderLoading } = useGetOrderByIdService(
    row?.id
  );

  return {
    order,
    orderLoading,
  };
};

export default useOrder;
