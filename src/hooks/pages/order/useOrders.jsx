import { useState } from "react";
import useModal from "@/hooks/commons/useModal";
import { useGetOrdersService } from "@/services/orders/orderService";
import Loader from "@/components/common/Loader";
import IconComponent from "@/components/common/Icon";
import { AiOutlineEye, AiOutlinePlusCircle } from "react-icons/ai";

const useOrders = () => {
  const [row, setRow] = useState(null);
  const [order, setOrder] = useState(null);
  const { data: orders, isLoading } = useGetOrdersService();

  const {
    isOpen: isOpenOrderModal,
    openModal: openOrderModal,
    closeModal: closeOrderModal,
  } = useModal();

  const {
    isOpen: isOpenFormModal,
    openModal: openFormModal,
    closeModal: closeFormModal,
  } = useModal();

  const onSelectOrder = (data) => {
    setRow(data);
    openOrderModal();
    console.log(data);
  };

  const onAddOrder = (id) => {
    setOrder(id);
    openFormModal();
  };

  const bodyOrderTable = () => (
    <tbody>
      {!row?.items?.length ? (
        <Loader />
      ) : (
        row?.items?.map((item) => (
          <tr key={item.id} className="text-left odd:bg-white even:bg-body">
            <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-gray-700 ">
              {item.id}
            </th>
            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ">
              {item.name}
            </td>
            <td className="border-t-0 px-6 align-center border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
              {item.quantity}
            </td>
            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
              ${item?.price}
            </td>
          </tr>
        ))
      )}
    </tbody>
  )

  const bodyOrdersTable = () => (
    <tbody>
      {!orders?.orders?.length ? (
        <Loader />
      ) : (
        orders?.orders?.map((item) => (
          <tr key={item.id} className="text-left odd:bg-white even:bg-body">
            <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-gray-700 ">
              {item.id}
            </th>
            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ">
              {item.name}
            </td>
            <td className="border-t-0 px-6 align-center border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
              {item.items?.length}
            </td>
            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
              ${item?.totals?.total}
            </td>
            <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 flex gap-3 flex-col md:flex-row">
              <button onClick={() => onSelectOrder(item)}>
                <IconComponent
                  icon={AiOutlineEye}
                  color="indigo-500"
                  size="lg"
                />
              </button>
              <button onClick={() => onAddOrder(item)}>
                <IconComponent
                  icon={AiOutlinePlusCircle}
                  color="indigo-500"
                  size="lg"
                />
              </button>
            </td>
          </tr>
        ))
      )}
    </tbody>
  );

  return {
    row,
    orders,
    order,
    isLoading,
    onAddOrder,
    onSelectOrder,
    bodyOrderTable,
    bodyOrdersTable,

    openOrderModal,
    closeOrderModal,
    isOpenOrderModal,

    openFormModal,
    closeFormModal,
    isOpenFormModal,
  };
};

export default useOrders;
