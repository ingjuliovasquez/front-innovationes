import axios from "axios";
import { handleError } from "@/utils/handleError";

const axiosInstance = axios.create({
  baseURL:
    import.meta.env.VITE_API_URL || "https://eshop-deve.herokuapp.com/api/v2",
});

axiosInstance.interceptors.request.use((config) => {
  const accessToken =
    import.meta.env.VITE_API_TOKEN ||
    "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJwUGFINU55VXRxTUkzMDZtajdZVHdHV3JIZE81cWxmaCIsImlhdCI6MTYyMDY2Mjk4NjIwM30.lhfzSXW9_TC67SdDKyDbMOYiYsKuSk6bG6XDE1wz2OL4Tq0Og9NbLMhb0LUtmrgzfWiTrqAFfnPldd8QzWvgVQ";
  config.headers.Authorization = `Bearer ${accessToken}`;
  return config;
});

axiosInstance.interceptors.response.use((response) => response, handleError);

export default axiosInstance;
