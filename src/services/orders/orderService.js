import { useGET, usePOST } from "@/utils/api";
import { showToastSuccess } from "@/utils/toast";

export const useGetOrdersService = () => useGET({ url: "/orders" });

export const useGetOrderByIdService = (id) => useGET({ url: `/orders/${id}` });

export const useAddOrderService = () =>
  usePOST({
    url: "/orders",
    onSuccess: () => {
        showToastSuccess('Registro insertado correctamente')
    }
  });
