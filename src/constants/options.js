export const specialist = [
  {
    value: "general",
    name: "Médico general",
  },
  {
    value: "Dentista",
    name: "Médico dentista",
  },
  {
    value: "psicologo",
    name: "Psicologo",
  },
];

export const tableHeaders = [
  {
    value: 'sku',
    name: 'SKU',
  },
  {
    value: 'order',
    name: 'Orden'
  },
  {
    value: 'items',
    name: 'Artículos'
  },
  {
    value: 'total',
    name: 'Total'
  },
  {
    value: 'actions',
    name: 'Acciones'
  },
]