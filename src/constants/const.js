export const FIELDS_COMPLETED = 'Todos los campos han sido completados';
export const FIELDS_REQUIRED = '* Campos obligatorios';
export const FIELD_REQUIRED = 'Campo obligatorio';
export const COMBO_REQUIRED = 'Seleccione una opción';
export const THREE_CHARACTERS_MINIMUN = 'Mínimo 3 caracteres';
export const MUST_BE_TWELVE_CHARACTERS = 'Deben de ser 12 caracteres';
export const MUST_BE_THIRTEEN_CHARACTERS = 'Deben de ser 13 caracteres';
export const MESSAGE_ERROR = 'Ha ocurrido un error';