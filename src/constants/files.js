import logo from "@/assets/icons/hospital.svg";
import logoDark from "@/assets/icons/hospital-dark.svg";
import doctor from "@/assets/ilustrations/doctor.svg";
import doctors from "@/assets/ilustrations/doctors.svg";
import medicalCare from "@/assets/ilustrations/medical_care.svg";
import medicalResearch from "@/assets/ilustrations/medical_research.svg";
import medicine from "@/assets/ilustrations/medicine.svg";

export {
  logo,
  logoDark,
  doctor,
  doctors,
  medicalCare,
  medicalResearch,
  medicine,
};
