import Layout from "@/components/layout/Layout";
import Home from "@/pages/Home";
import Order from "@/pages/orders/Order";
import { createBrowserRouter } from "react-router-dom";

const mainRouter = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/order/:number",
        element: <Order />,
      },
    ],
  },
]);

export default mainRouter;
