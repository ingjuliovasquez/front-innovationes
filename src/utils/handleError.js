import { showToastError } from "@/utils/toast";
import { MESSAGE_ERROR } from "@/constants/const";

export const handleError = async (error) => {
  if (error.response?.status === 401) {
    const keys = Object.keys(error.response?.data?.errors);
    keys.forEach((key) => {
      showToastError(error.response?.data?.errors[key][0])
    });
  } else if (error.response?.status === 400) {
    try {
      const { data } = error.response;
      if (data?.title) {
        const { title, detail } = data;
        console.log(title, detail);
        // showToastError(<ErrorToastApi title={title} detail={detail} />);
      } else {
        const keys = Object.keys(data.errors);
        if (keys.length > 0) {
          const values = Object.values(data.errors);
          showToastError(`${keys[0]}: ${values[0]}`);
        } else {
          const { message } = data;
          showToastError(message);
        }
      }
    } catch (_error) {
      showToastError(MESSAGE_ERROR);
    }
  } else if (error.response?.status === 404) {
    // Todo: Pendiente
  } else {
    console.log("Error 500");
    const { response } = error;
    if (response?.data?.message) {
      const { message } = response.data;
      showToastError(message);
    } else {
      showToastError(MESSAGE_ERROR);
    }
  }

  return Promise.reject(error);
};
