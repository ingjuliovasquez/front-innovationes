import Section from "@/components/layout/Section";
import Card from "@/components/common/Card";
import Table from "@/components/common/Table";
import Text from "@/components/common/Text";
import Flex from "@/components/common/Flex";
import Button from "@/components/common/Button";
import OrderModal from "@/pages/orders/OrderModal";
import useOrders from "@/hooks/pages/order/useOrders";
import Order from "./orders/Order";

const Home = () => {
  const {
    closeFormModal,
    closeOrderModal,
    isOpenFormModal,
    isOpenOrderModal,
    openFormModal,
    row,
    order,
    bodyOrdersTable,
    bodyOrderTable
  } = useOrders();
  return (
    <>
      <Section id="home-id">
        <Card>
          <Flex className="px-8 py-3" justify="between">
            <Text color="indigo-800" size="2xl" weight="semibold">
              Tabla de ordenes
            </Text>
            <Button onClick={openFormModal}>Agregar</Button>
          </Flex>

          <Table
            tbody={bodyOrdersTable}
          />
        </Card>
      </Section>
      <OrderModal
        order={order}
        onEnd={closeFormModal}
        isShow={isOpenFormModal}
        onCancel={closeFormModal}
      />
      <Order 
        row={row} 
        bodyOrderTable={bodyOrderTable} 
        isOpen={isOpenOrderModal} 
        onClose={closeOrderModal} 
        />
    </>
  );
};

export default Home;
