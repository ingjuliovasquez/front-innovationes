import React from "react";
import OrderForm from "./OrderForm";
import Modal from "@/components/common/Modal";

const OrderModal = ({ 
  isShow, 
  onCancel, 
  isUpdate, 
  onEnd, 
  order }) => {
  return (
    <Modal
      onClose={onCancel}
      isShow={isShow}
      title={isUpdate ? "Modificar producto" : "Agregar producto"}
    >
      <OrderForm order={order} onCancel={onCancel} onEnd={onEnd} />
    </Modal>
  );
};

export default OrderModal;
