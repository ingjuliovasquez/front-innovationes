import Flex from "@/components/common/Flex";
import Form from "@/components/common/Form";
import Input from "@/components/common/Input";
import InputHiddenController from "@/components/common/InputHidden";
import { fieldRequired } from "@/constants/validations";
import useOrderForm from "@/hooks/pages/order/useOrderForm";
import { Controller } from "react-hook-form";

const OrderForm = ({ order, onEnd }) => {
  const { control, errors, handleSubmit, onSubmit } = useOrderForm({order, onEnd});
  return (
    <Form onSubmit={handleSubmit(onSubmit)} errors={errors} className="px-10">
      <Input
        control={control}
        label="Nombre"
        name="name"
        placeholder="Nombre del producto"
        rules={fieldRequired}
      />
      <Flex gap={30}>
        <Input
          control={control}
          label="Cantidad"
          name="quantity"
          type="number"
          placeholder="Cantidad"
          rules={fieldRequired}
        />
        <Input
          control={control}
          label="Precio"
          name="price"
          placeholder="Precio"
          rules={fieldRequired}
        />
      </Flex>

      <div className="relative mb-16">
        <button
          className="absolute top-3 right-0 rounded-lg bg-primary text-lg p-3 shadow-md text-gray-600"
          type="submit"
        >
          Guardar
        </button>
      </div>
    </Form>
  );
};

export default OrderForm;
