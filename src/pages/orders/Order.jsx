import CircleLoader from "@/components/common/CircleLoader";
import Modal from "@/components/common/Modal";
import Table from "@/components/common/Table";

const Order = ({ isOpen, onClose, bodyOrderTable, row = null }) => (
  <Modal title={`Orden ${row?.name}`} isShow={isOpen} onClose={onClose}>
    {!row ? (
      <div className="h-40">
        <CircleLoader />
      </div>
    ) : (
      <div>
        <div className="header___order mb-4">
          <p className="text-lg">
            Cliente:{" "}
            {`${row?.shippingAddress?.firstName} ${row?.shippingAddress?.lastName} `}
          </p>
          <p className="text-lg">
            Dirección:{" "}
            {`${row?.shippingAddress?.address1}, ${row?.shippingAddress?.state?.name}, ${row?.shippingAddress?.country?.code}`}
          </p>
          <p className="text-lg">
            Teléfono: {`${row?.shippingAddress?.phone}`}
          </p>
        </div>
        <div className="header___body">
          <Table
            headers={[
              {
                value: "sku",
                name: "SKU",
              },
              {
                value: "name",
                name: "Nombre",
              },
              {
                value: "quantity",
                name: "Cantidad",
              },
              {
                value: "price",
                name: "Precio",
              },
            ]}
            tbody={bodyOrderTable} />
        </div>
      </div>
    )}
  </Modal>
);

export default Order;
