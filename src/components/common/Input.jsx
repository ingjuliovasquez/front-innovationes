import { Controller } from "react-hook-form";

const Input = ({
  name = "",
  label = "",
  placeholder = "",
  textArea = false,
  control,
  rules = {},
  defaultValue,
  className = "",
  onChange = () => {},
  type = "text",
  valueId = '',
  ...props
}) => (
  <>
    <Controller
      name={name}
      control={control}
      rules={rules}
      defaultValue={defaultValue}
      render={({
        field: { onChange: onChangeField, value },
        fieldState: { error },
      }) => (
        <div className="mb-4 w-full">
          <label className="mb-1 block font-medium text-gray-500">
            {label}
          </label>
          <input
            value={value ?? ""}
            id={`textfield-${name}`}
            onChange={(event) => {
              onChangeField(event.target.value);
              onChange(event.target.value);
            }}
            type={type}
            placeholder={placeholder}
            className={`w-full rounded-lg border-2 ${
              error ? "border-red-600" : "border-black/20"
            } bg-transparent py-3 px-5 font-medium outline-none transition focus:border-indigo-500 active:border-indigo-500 disabled:cursor-default disabled:bg-whiter dark:bg-form-input ${className}`}
            {...props}
          />
          {error && <p className="text-red-600 font-medium">{error.message}</p>}
        </div>
      )}
    />
  </>
);

export default Input;
