import "@/assets/styles/loader.css";

const Loader = () => (
  <tr>
    <th colSpan={5} className="py-10 px-24">
      <span className="loader"></span>
    </th>
  </tr>
);

export default Loader;
