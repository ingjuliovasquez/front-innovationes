import React from "react";

const Form = ({
  className = "",
  isLoading,
  onSubmit,
  children,
  errors = {},
  ...props
}) => {
  if (isLoading) return <h2>Cargando...</h2>;
  return (
    <form className={className} onSubmit={onSubmit} {...props}>
      {children}
    </form>
  );
};

export default Form;
