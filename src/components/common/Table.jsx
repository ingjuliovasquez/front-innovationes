import { AiOutlineEye, AiOutlinePlusCircle } from "react-icons/ai";
import { tableHeaders } from "@/constants/options";
import Loader from "@/components/common/Loader";
import IconComponent from "./Icon";
import Order from "@/pages/orders/Order";

const Table = ({
  headers = tableHeaders ?? [],
  tbody
}) => {
  return (
  <>
    <table className="items-center bg-transparent w-full border-collapse ">
      <thead>
        <tr>
          {headers.map((item) => (
            <th
              key={item.value}
              className="px-6 bg-indigo-50 text-indigo-500 align-middle border border-solid border-indigo-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left"
            >
              {item.name}
            </th>
          ))}
        </tr>
      </thead>

      
      {tbody()}

    </table>
  </>
)};

export default Table;
