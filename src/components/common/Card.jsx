const Card = ({ children, padding = 0 }) => {
  return (
    <div className={`bg-white shadow mx-auto rounded-lg ${padding}`}>
      {children}
    </div>
  );
};

export default Card;
