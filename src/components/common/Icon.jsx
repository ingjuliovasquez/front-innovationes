
const IconComponent = ({ icon: IconHtml, color = "white", size = "2xl", className = "", ...props }) => {
    return <IconHtml className={`text-${size} text-${color} ${className}`} {...props} />;
};

export default IconComponent;
