import React from "react";

const Text = ({
  children,
  size = "lg",
  color = "black",
  padding = 2,
  weight = "",
}) => {
  return (
    <div className={`font-${weight} text-${size} text-${color} p-${padding}`}>{children}</div>
  );
};

export default Text;
