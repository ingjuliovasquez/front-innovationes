import "@/assets/styles/circle-loader.css";

const CircleLoader = () => (
  <div className="absolute top-[60%] left-[50%] -translate-x-[50%] -translate-y-[60%]">
    <span className="shape-loader"></span>
  </div>
);

export default CircleLoader;
