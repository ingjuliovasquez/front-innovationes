import { Controller } from "react-hook-form";
const InputHiddenController = ({ name = "", control, inputValue }) => (
  <Controller
    name={name}
    control={control}
    render={({ field: { value = inputValue ?? '' } }) => {
      console.log(value);
      return <input type="hidden" value={value} />;
    }}
  />
);

export default InputHiddenController;
