import React, { useEffect, useRef } from "react";
import IconComponent from "@/components/common/Icon";
import { IoCloseCircleOutline } from "react-icons/io5";

const Modal = ({ title = "Modal", isShow = false, onClose, children }) => 
{

  return (
  <div
    className={`fixed w-full h-screen top-0 left-0 bg-black/40 ${
      isShow ? "block" : "hidden"
    }`}
  >
    <div className="fixed bg-white shadow-lg rounded-lg p-10 z-10 w-[60%] mx-auto top-[60%] left-[50%] -translate-x-[50%] -translate-y-[60%]">
      <span className="absolute top-0 right-0 p-10" onClick={onClose}>
        <IconComponent
          icon={IoCloseCircleOutline}
          color="text-dark"
          size="3xl"
          className="cursor-pointer"
        />
      </span>
      <h3 className="text-3xl font-medium text-left mb-5">{title}</h3>
      {children}
    </div>
  </div>
)}

export default Modal;
