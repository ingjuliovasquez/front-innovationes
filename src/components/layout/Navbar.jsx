import { Link } from "react-router-dom"
import logo from "@/assets/icons/logo.svg"
const Navbar = () => {
  return (
    <nav className="sticky top-0 z-30 flex items-center justify-between py-5 px-5 lg:px-20 shadow bg-white transition-all delay-100 duration-200 ease-in-out">
      <Link>
        <img src={logo} alt="Icono de logo" className="w-40" />
      </Link>
    </nav>
  )
}

export default Navbar