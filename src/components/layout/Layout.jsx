import React from 'react'
import Header from '@/components/layout/Header'
import Breadcrumb from '@/components/layout/Breadcrumb'
import { Outlet } from 'react-router-dom'

const Layout = () => {
  return (
    <div className='bg-body'>
        <Header />
        <Outlet />
    </div>
  )
}

export default Layout