
const Section = ({children, id = '', className = ''}) => {
  return (
    <div id={id} className={`px-5 py-5 lg:py-10 lg:px-20 ${className}`}>
      {children}
    </div>
  )
}

export default Section