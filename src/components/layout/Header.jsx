import Navbar from "@/components/layout/Navbar";

const Header = ({ className = "" }) => {
  return (
    <header className={`${className}`}>
      <Navbar />
    </header>
  );
};

export default Header;
