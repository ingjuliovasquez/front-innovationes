import React from "react";

const Breadcrumb = ({ prevLinks = [], title = "", titleCustom }) => {
  return (
    <nav>
      <ol>
        {prevLinks.forEach((item) => (
          <li key={item.value} className="text-indigo-700">
            <Link to={item.to}>{item.name}</Link>
          </li>
        ))}
        <li className="text-indigo-800">{titleCustom || title}</li>
      </ol>
    </nav>
  );
};

export default Breadcrumb;
